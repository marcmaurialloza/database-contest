/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcoracle;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import sun.security.util.BigInt;

/**
 *
 * @author Neville
 */
public class JDBCOracle {

    private static String randomString(int length, Random random) {
        String c = new String();
        String caracteres = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < length; ++i) {
            c += caracteres.charAt(random.nextInt(caracteres.length()));
        }
        return c;
    }
    
    private static String randomDate(Random random) {
        int anyo, mes, dia;
        anyo = random.nextInt(14) + 2000;
        mes = random.nextInt(12) + 1;
        dia = random.nextInt(28) + 1;
        return String.valueOf(anyo)+"-"+String.valueOf(mes)+"-"+String.valueOf(dia);
    }
    
    private static Connection connect() throws SQLException {
        Properties props = new Properties();
        props.setProperty("user", "antonio.suarez.galan");
        props.setProperty("password", "DB300690");
        
        return DriverManager.getConnection("jdbc:oracle:thin:@oraclefib.fib.upc.es:1521:ORABD",props);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            double sf = 0.0066667;
            int limite_rango = 0;
            
            Connection c = connect();
            c.setAutoCommit(false);
            
            insertsRegion(c); 
            limite_rango += 5;
            insertsNation(c); 
            limite_rango += 25;
            insertsSupplier(c,sf,limite_rango);
            limite_rango += sf*10000;
            insertsCustomer(c,sf,limite_rango);
            limite_rango += sf*150000;
            insertsParts(c,sf,limite_rango);
            limite_rango += sf*200000;
            insertsPartsUpp(c,sf,limite_rango);
            insertsOrders(c,sf,limite_rango);
            limite_rango += sf*1500000;
            insertsLineItems(c,sf,limite_rango);
            c.commit();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCOracle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void insertsRegion(Connection c) throws SQLException {
        /*        REGION Table Layout
        Column Name     Datatype Requirements       Comment
        R_REGIONKEY     number(38,0)                 5 regions are populated
        R_NAME          varchar2(64)         
        R_COMMENT       variable text, size 152
        Primary Key: R_REGIONKEY*/
        Random random = new Random(42);
        PreparedStatement ps = c.prepareStatement("Insert into region values(?,?,?,?)");
        for (int i = 0; i < 5; ++i) {
            //System.out.println(i);
            BigDecimal aux = new BigDecimal("1000000000000000000"+Integer.toString(i));
            ps.setBigDecimal(1, aux);
            ps.setString(2, randomString(32,random));
            ps.setString(3, randomString(80,random));
            ps.setString(4, "");
            ps.addBatch();
        }
        ps.executeBatch();
        ps.close();
    }

    private static void insertsNation(Connection c) throws SQLException {
        /*Column Name     Datatype Requirements       Comment
        N_NATIONKEY       Number(38,0)                25 nations are populated
        N_NAME            Varchar2(64)         
        N_REGIONKEY       Number(38,0)                Foreign Key to R_REGIONKEY  
        N_COMMENT         Varchar(160)     
        Primary Key: N_NATIONKEY*/
        Random myrandom = new Random(42);
        PreparedStatement ps = c.prepareStatement("Insert into nation values(?,?,?,?,?)");
        for (int i = 5; i < 30; ++i) {
            BigDecimal aux = new BigDecimal("1000000000000000000"+Integer.toString(i));
            ps.setBigDecimal(1, aux);
            ps.setString(2, randomString(32,myrandom));
            ps.setBigDecimal(3, new BigDecimal("1000000000000000000"+myrandom.nextInt(5)));
            ps.setString(4, randomString(80,myrandom));
            ps.setString(5, "");
            ps.addBatch();
        }
        ps.executeBatch();
        ps.close();
    }

    private static void insertsSupplier(Connection c, double sf, int par) throws SQLException {
/*       Column Name    Datatype Requirements   Comment
 *       S_SUPPKEY      Number(38,0)              SF*10,000 are populated
 *       S_NAME         Varchar2(64)
 *       S_ADDRESS      Varchar2(64)
 *       S_NATIONKEY    Number(38,0)              Foreign Key to N_NATIONKEY
 *       S_PHONE        Varchar2(18)     
 *       S_ACCTBAL      Number(13,2)                 
 *       S_COMMENT      Varchar2(105)
 *       Primary Key: S_SUPPKEY*/
        Random random = new Random(42);
        PreparedStatement ps = c.prepareStatement("Insert into supplier values(?,?,?,?,?,?,?,?)");
        for (int i = 0+par; i < ((int)(sf*10000))+par; ++i) {
          //  System.out.println(i-par);
            ps.setBigDecimal(1,new BigDecimal("1000000000000000000"+Integer.toString(i)));
            ps.setString(2, randomString(32,random));
            ps.setString(3, randomString(32,random));
            ps.setBigDecimal(4, new BigDecimal("1000000000000000000"+Integer.toString(random.nextInt(25) + 5)));
            ps.setString(5, randomString(9,random));
            ps.setBigDecimal(6, new BigDecimal("99999"+Integer.toString(random.nextInt(99999))));
            ps.setString(7, randomString(52,random));
            ps.setString(8, "");
            ps.addBatch();
        }
        ps.executeBatch();
        ps.close();
    }

    private static void insertsCustomer(Connection c, double sf, int par) throws SQLException {
/*      Column Name     Datatype Requirements   Comment
 *      C_CUSTKEY       Number(38,0)              SF*150,000 are populated
 *      C_NAME          Varchar2(64)
 *      C_ADDRESS       Varchar2(64)
 *      C_NATIONKEY     Number(38,0)              Foreign Key to N_NATIONKEY
 *      C_PHONE         Varchar2(64)
 *      C_ACCTBAL       Number(13,2)
 *      C_MKTSEGMENT    Varchar2(64)
 *      C_COMMENT       Varchar2(120)
 *      Primary Key: C_CUSTKEY*/
        Random random = new Random(42);
        PreparedStatement ps = c.prepareStatement("Insert into customer values(?,?,?,?,?,?,?,?,?)");
        for (int i = 0+par; i < ((int)(sf*150000))+par; ++i) {
             ps.setBigDecimal(1,new BigDecimal("1000000000000000000"+Integer.toString(i)));
             ps.setString(2, randomString(32,random));
             ps.setString(3, randomString(32,random));
             ps.setBigDecimal(4, new BigDecimal("1000000000000000000"+Integer.toString(random.nextInt(25) + 5)));
             ps.setString(5, randomString(32,random));
             ps.setBigDecimal(6, new BigDecimal("99999"+Integer.toString(random.nextInt(99999))));
             ps.setString(7, randomString(32,random));
             ps.setString(8, randomString(60,random));
             ps.setString(9, "");
             ps.addBatch();
        }
        ps.executeBatch();
        ps.close();
        
    }

    private static void insertsParts(Connection c, double sf, int par) throws SQLException {
/*      Column Name     Datatype Requirements   Comment
 *      P_PARTKEY       Number(38,0)            SF*200,000 are populated
 *      P_NAME          Varchar2(64)
 *      P_MFGR          Varchar2(64)
 *      P_BRAND         Varchar2(64)
 *      P_TYPE          Varchar2(64)
 *      P_SIZE          Number(38,0)
 *      P_CONTAINER     Varchar2(64)
 *      P_RETAILPRICE   Number(13,2)
 *      P_COMMENT       Varchar2(64)
 *      Primary Key: P_PARTKEY*/
        Random random = new Random(42);
        PreparedStatement ps = c.prepareStatement("Insert into part values(?,?,?,?,?,?,?,?,?,?)");
         for (int i = 0+par; i < ((int)(sf*200000))+par; ++i) {
             ps.setBigDecimal(1,new BigDecimal("1000000000000000000"+Integer.toString(i)));
             ps.setString(2, randomString(32,random));
             ps.setString(3, randomString(32,random));
             ps.setString(4, randomString(32,random));
             ps.setString(5, randomString(32,random));
             ps.setBigDecimal(6,new BigDecimal("555555555555555555"+Integer.toString(random.nextInt(999999))));
             ps.setString(7, randomString(32,random));
             ps.setBigDecimal(8, new BigDecimal("77777"+Integer.toString(random.nextInt(99999))));
             ps.setString(9, randomString(32,random));
             ps.setString(10, "");
             ps.addBatch();
         }
         ps.executeBatch();
         ps.close();
    }

    private static void insertsPartsUpp(Connection c, double sf, int par) throws SQLException {
/*      Column Name     Datatype Requirements   Comment
 *      PS_PARTKEY      Number(38,0)              Foreign Key to P_PARTKEY
 *      PS_SUPPKEY      Number(38,0)              Foreign Key to S_SUPPKEY
 *      PS_AVAILQTY     Number(38,0)
 *      PS_SUPPLYCOST   Number(13,2)
 *      PS_COMMENT      Varchar2(200)
 *      Primary Key: PS_PARTKEY, PS_SUPPKEY*/
        int p_min, p_max;
        int s_min, s_max;
        s_min = 30; s_max = ((int) (sf*10000)) + 30;
        p_min = 30 + ((int) (sf*10000)) + ((int) (sf*150000));
        p_max = p_min + ((int) (sf*200000));
        int p,s;
        p = p_min;
        s = s_min;
        Random random = new Random(42);
        PreparedStatement ps = c.prepareStatement("Insert into partsupp values(?,?,?,?,?,?)");
        for (int i = 0+par; i < ((int)(sf*800000))+par; ++i) {
            ps.setBigDecimal(1,new BigDecimal("1000000000000000000"+Integer.toString(p)));
            ps.setBigDecimal(2,new BigDecimal("1000000000000000000"+Integer.toString(s)));
            ++s;
            if (s >= s_max) {
                s = s_min;
                ++p;
            }
            ps.setBigDecimal(3,new BigDecimal("111111111111111111"+random.nextInt(99999)));
            ps.setBigDecimal(4, new BigDecimal("77777"+Integer.toString(random.nextInt(99999))));
            ps.setString(5, randomString(100,random));
            ps.setString(6, "");
            ps.addBatch();
        }
        ps.executeBatch();
        ps.close();
    }

    private static void insertsOrders(Connection c, double sf, int par) throws SQLException {
/*      Column Name     Datatype Requirements       Comment
 *      O_ORDERKEY      Number(38,0)                SF*1,500,000 are sparsely populated
 *      O_CUSTKEY       Number(38,0)                Foreign Key to C_CUSTKEY
 *      O_ORDERSTATUS   Varchar2(64)
 *      O_TOTALPRICE    Number(13,2)
 *      O_ORDERDATE     Date
 *      O_ORDERPRIORITY Varchar2(15)
 *      O_CLERK         Varchar2(64)
 *      O_SHIPPRIORITY  Number(38,0)
 *      O_COMMENT       Varchar2(80)
 *      Primary Key: O_ORDERKEY*/
        PreparedStatement ps = c.prepareStatement("Insert into orders values(?,?,?,?,to_date(?, 'YYYY/mm/dd'),?,?,?,?,?)");
        Random random = new Random(42);
        for (int i = 0+par; i < ((int)(sf*1500000))+par; ++i) {
            ps.setBigDecimal(1,new BigDecimal("1000000000000000000"+Integer.toString(i)));
            ps.setBigDecimal(2,new BigDecimal("1000000000000000000"+Integer.toString(random.nextInt((int)(sf*100000)) + (30 + (int)(sf*10000)))));
            ps.setString(3, randomString(32,random));
            ps.setBigDecimal(4,new BigDecimal("22222"+Integer.toString(random.nextInt(999999))));
            ps.setString(5, randomDate(random));
            ps.setString(6, randomString(7,random));
            ps.setString(7, randomString(32,random));
            ps.setBigDecimal(8,new BigDecimal("333333333333333333"+Integer.toString(random.nextInt(999999))));
            ps.setString(9, randomString(40,random));
            ps.setString(10,"");
            ps.addBatch();
        }
        ps.executeBatch();
        ps.close();
    }

    private static void insertsLineItems(Connection c, double sf, int par) throws SQLException {
 /*     Column Name     Datatype Requirements       Comment
  *  1   L_ORDERKEY      Number(38,0)                Foreign Key to O_ORDERKEY
  *  2   L_PARTKEY       Number(38,0)                Foreign key to P_PARTKEY, first part of the compound Foreign Key to (PS_PARTKEY, PS_SUPPKEY) with L_SUPPKEY
  *  3   L_SUPPKEY       Number(38,0)                Foreign key to S_SUPPKEY, second part of the compound Foreign Key to (PS_PARTKEY, PS_SUPPKEY) with L_PARTKEY
  *  4   L_LINENUMBER    Number(38,0)                 
  *  5   L_QUANTITY      Number(38,0)
  *  6   L_EXTENDEDPRICE Number(13,2)
  *  7   L_DISCOUNT      Number(13,2)
  *  8   L_TAX           Number(13,2)
  *  9   L_RETURNFLAG    Varchar2(64)
  *  10   L_LINESTATUS    Varchar2(64)
  *  11   L_SHIPDATE      date
  *  12   L_COMMITDATE    date
  *  13   L_RECEIPTDATE   date
  *  14   L_SHIPINSTRUCT  Varchar2(64)
  *  15   L_SHIPMODE      Varchar2(64)
  *  16   L_COMMENT       Varchar2(64)
  *     Primary Key: L_ORDERKEY, L_LINENUMBER*/
        int p_min, p_max;
        int s_min, s_max;
        s_min = 30; s_max = ((int) (sf*10000)) + 30;
        p_min = 30 + ((int) (sf*10000)) + ((int) (sf*150000));
        p_max = p_min + ((int) (sf*200000));
        int p,s;
        p = p_min;
        s = s_min;
        Random random = new Random(42);
        PreparedStatement ps = c.prepareStatement("Insert into lineitem values(?,?,?,?,?,?,?,?,?,?,to_date(?, 'YYYY/mm/dd'),to_date(?, 'YYYY/mm/dd'),to_date(?, 'YYYY/mm/dd'),?,?,?,?)");
        int min_orders = par - (int)((sf*1500000));
        for (int i = 0+par; i < ((int)(sf*6000000))+par; ++i) {
            ps.setBigDecimal(1,new BigDecimal("1000000000000000000"+(random.nextInt((int)(sf*1500000))+min_orders)));
            ps.setBigDecimal(2,new BigDecimal("1000000000000000000"+Integer.toString(p)));
            ps.setBigDecimal(3,new BigDecimal("1000000000000000000"+Integer.toString(s)));
            ++s;
            if (s > s_max) {
                s = s_min;
                ++p;
            }
            ps.setBigDecimal(4, new BigDecimal("1000000000000000000"+Integer.toString(i)));
            ps.setBigDecimal(5, new BigDecimal("1111111111111111111"+Integer.toString(random.nextInt(999999))));
            ps.setBigDecimal(6, new BigDecimal("22222"+Integer.toString(random.nextInt(999999))));
            ps.setBigDecimal(7, new BigDecimal("33333"+Integer.toString(random.nextInt(999999))));
            ps.setBigDecimal(8, new BigDecimal("44444"+Integer.toString(random.nextInt(999999))));
            ps.setString(9, randomString(32,random));
            ps.setString(10, randomString(32,random));
            ps.setString(11, randomDate(random));
            ps.setString(12, randomDate(random));
            ps.setString(13, randomDate(random));
            ps.setString(14, randomString(32,random));
            ps.setString(15, randomString(32,random));
            ps.setString(16, randomString(32,random));
            ps.setString(17, "");
            ps.addBatch();
        }
        ps.executeBatch();
        ps.close();
    }
}
